#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void main(){

    int menuinicial=0, menupesquisa=0, menualteracao1=0, menualteracao2=0,verificador=0;

    do{
        printf ("\e[1;1H\e[2J");
        printf ("Digite a opcao: \n");
        printf ("Digite 1 para cadastrar livro.\n");
        printf ("Digite 2 para pesquisar livro por nome. \n");
        printf ("Digite 3 para pesquisar livro por disciplinas.\n");
        printf ("Digite 4 para sair do programa.");


        scanf ("%d",& menuinicial);

        switch(menuinicial){
            case 1:
                do{
                    verificador=1;
                    printf ("\e[1;1H\e[2J");
                    printf ("Cadastro de livros\n\n");
                    printf ("\nDigite o nome do livro: ");
                    printf ("\nDigite o autor do livro: ");
                    printf ("\nDigite a editora: ");
                    printf ("\nDigite a edicao: ");
                    printf ("\nDigite o ano de lancamento: ");
                    printf ("\nDigite o numero de paginas: ");
                    printf ("\nDigite a disciplina que o livro pertence: ");
                    printf ("\nLivro cadastrado com sucesso.");
                    printf ("\nDigite 1 para cadastrar outro livro.");
                    printf ("\nDigite 2 para voltar ao menu inicial.");
                    printf ("\nDigite 3 para sair do programa.");
                    scanf ("%d",& menupesquisa);
                    switch(menupesquisa){
                        case 1:
                            verificador=1;
                        break;
                        case 2:
                            verificador=0;
                        break;
                        case 3:
                            verificador=2;
                        break;

                    }
                }while(verificador==1);
                break;
            case 2:
                do{
                    verificador=1;
                    printf ("\e[1;1H\e[2J");
                    printf ("Pesquisa de livros por nome.\n");
                    printf ("\nDigite o nome do livro: ");
                    printf ("\nLivro encontrado com sucesso.");
                    printf ("\nDigite 1 para pesquisar outro livro.");
                    printf ("\nDigite 2 para alterar o livro.");
                    printf ("\nDigite 3 para voltar ao menu inicial.");
                    printf ("\nDigite 4 para sair do programa.");
                    scanf ("%d",& menupesquisa);
                    switch(menupesquisa){
                        case 1:
                            verificador=1;
                        break;
                        case 2:
                            do{
                                verificador=2;
                                printf ("\e[1;1H\e[2J");
                                printf ("Alteracao de livros.\n");
                                printf ("\nDigite 1 para alterar o nome do livro");
                                printf ("\nDigite 2 para alterar o autor. ");
                                printf ("\nDigite 3 para alterar a editora.");
                                printf ("\nDigite 4 para alterar a edicao.");
                                printf ("\nDigite 5 para alterar o ano de lancamento.");
                                printf ("\nDigite 6 para alterar o numero de paginas.");
                                printf ("\nDigite 7 para alterar a disciplina.");
                                printf ("\nDigite 8 para voltar ao menu inicial.");
                                printf ("\nDigite 9 para sair do programa.");
                                scanf ("%d",& menualteracao1);
                                switch(menualteracao1){
                                    case 1:
                                        printf ("\nDigite o nome do livro: ");
                                        printf ("\nNome alterado com sucesso, voltando ao menu anterior.");
                                        verificador=2;
                                        __fpurge(stdin);
                                        getchar();
                                    break;
                                    case 8:
                                        printf ("\nVoltando ao menu inicial.");
                                        verificador=0;
                                        __fpurge(stdin);
                                        getchar();
                                    break;
                                    case 9:
                                        printf ("\nSaindo do programa.");
                                        verificador=2;
                                    break;
                                        
                                }
                            }while(verificador==2);
                        break;
                        case 3:
                            verificador=0;
                        break;
                        case 4:
                            verificador=2;
                        break;

                    }

                }while(verificador==1);
            break;
            case 3:
                do{
                    verificador=1;
                    printf ("\e[1;1H\e[2J");
                    printf ("Pesquisa de livros por disciplina.\n");
                    printf ("\nDigite o nome do livro: ");
                    printf ("\nLivro encontrado com sucesso.");
                    printf ("\nDigite 1 para pesquisar outro livro.");
                    printf ("\nDogote 2 para alterar o livro: ");
                    printf ("\nDigite 3 para voltar ao menu inicial.");
                    printf ("\nDigite 4 para sair do programa.");
                    scanf ("%d",& menupesquisa);
                    switch(menupesquisa){
                        case 1:
                            verificador=1;
                        break;
                        case 2:
                            do{
                                verificador=2;
                                printf ("\e[1;1H\e[2J");
                                printf ("Alteracao de livros.\n");
                                printf ("\nDigite 1 para alterar o nome do livro");
                                printf ("\nDigite 2 para alterar o autor. ");
                                printf ("\nDigite 3 para alterar a editora.");
                                printf ("\nDigite 4 para alterar a edicao.");
                                printf ("\nDigite 5 para alterar o ano de lancamento.");
                                printf ("\nDigite 6 para alterar o numero de paginas.");
                                printf ("\nDigite 7 para alterar a disciplina.");
                                printf ("\nDigite 8 para voltar ao menu inicial.");
                                printf ("\nDigite 9 para sair do programa.");
                                scanf ("%d",& menualteracao2);
                                switch(menualteracao2){
                                    case 1:
                                        printf ("\nDigite o nome do livro:");
                                        printf ("\nNome alterado com sucesso, voltando ao menu anterior.");
                                        verificador=2;
                                        __fpurge(stdin);
                                        getchar();
                                    break;
                                    case 8:
                                        printf ("\nVoltando ao menu inicial.");
                                        verificador=1;
                                        __fpurge(stdin);
                                        getchar();
                                    break;
                                    case 9:
                                        printf ("\nSaindo do programa.");
                                        verificador=3;
                                        
                                }
                            }while(verificador==2);
                        break;
                        case 3:
                            verificador=0;
                        break;
                        case 4:
                            verificador=2;
                        break;

                    }

                }while(verificador==1);

            break;
            case 4:
                verificador=3;
            break;
            default:
                printf ("\nOpcao digitada invalida.");
                printf ("\nDigite qualquer tecla para continuar.");
                __fpurge(stdin);
                getchar();
                verificador=0;
        }
    }while(verificador==0);
}
